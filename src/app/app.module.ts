import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from "@angular/common/http";
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DatabaseProvider } from '../providers/database/database';
import { ModaluserPage } from "../pages/modaluser/modaluser";
import { ModalindustryPage } from "../pages/modalindustry/modalindustry";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ModaluserPage,
    ModalindustryPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    // ModalindustryPage,
    // ModaluserPage,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ModaluserPage,
    ModalindustryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider
  ]
})
export class AppModule {}
