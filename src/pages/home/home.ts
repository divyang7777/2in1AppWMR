import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { DatabaseProvider } from "../../providers/database/database";
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ModaluserPage } from '../modaluser/modaluser';
import { ModalindustryPage } from '../modalindustry/modalindustry';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public DatabaseProvider: DatabaseProvider,
    public modalCtrl: ModalController
  ) {

  }

  openModaluser() {  
    let modal = this.modalCtrl.create(ModaluserPage);
      modal.present();
  }

  openModalindustry() { 
    let modal = this.modalCtrl.create(ModalindustryPage);
      modal.present();
  }


}
