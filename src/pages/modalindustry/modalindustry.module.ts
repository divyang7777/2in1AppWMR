import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalindustryPage } from './modalindustry';

@NgModule({
  declarations: [
    ModalindustryPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalindustryPage),
  ],
})
export class ModalindustryPageModule {}
