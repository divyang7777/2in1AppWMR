import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { DatabaseProvider } from "../../providers/database/database";
import { HomePage } from '../home/home';
// import { HomePage } from "../../pages/home/home";

@IonicPage()
@Component({
  selector: 'page-modalindustry',
  templateUrl: 'modalindustry.html',
})
export class ModalindustryPage {
  
  
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalindustryPage');
  }

  // toastCtrl: any;
  type: any;
  viewCtrl: any;
  summary: any;
  // area: any;
  userid: any;
  name: any;
  data : any;
  members: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public DatabaseProvider: DatabaseProvider,
    public toastCtrl: ToastController
  ) {

  }

  saveUsers() {
    // console.log("done request send !!!");
    const text = { 
    'members': this.members, 
    'name':  this.name,
    'summary': this.summary,
    // 'userid': this.userid, 
    'type': this.type };
    
    this.DatabaseProvider.addIndustry(text)
      .then(data => {
        console.log(data);
      }, (err) => {
        console.log(err);
      });
      this.navCtrl.setRoot(HomePage); 
 }

 showToast(){
  let toast = this.toastCtrl
  .create({
    message: 'Your Water is on the way, Stay tunned !!!',
    showCloseButton: true,
    closeButtonText: 'Ok'
  });
  toast.present();
 }

}

