import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModaluserPage } from './modaluser';

@NgModule({
  declarations: [
    ModaluserPage,
  ],
  imports: [
    IonicPageModule.forChild(ModaluserPage),
  ],
})
export class ModaluserPageModule {}
