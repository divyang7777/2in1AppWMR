import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { DatabaseProvider } from "../../providers/database/database";
import { HomePage } from "../../pages/home/home";
// import { Nav } from 'ionic-angular/components/nav/nav';

@IonicPage()
@Component({
  selector: 'page-modaluser',
  templateUrl: 'modaluser.html',
})
export class ModaluserPage {
  nav: any;
  appCtrl: any;
  viewCtrl: any;
  summary: any;
  area: any;
  userid: any;
  name: any;
  data: any;
  members: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public DatabaseProvider: DatabaseProvider
  ) {

  }

  saveUsers() {
    // console.log("done request send !!!");
    const text = {
      'members': this.members,
      'name': this.name,
      'summary': this.summary,
      'area': this.area
    };

    this.DatabaseProvider.addUser(text)
      .then(data => {
        console.log(data);
      }, (err) => {
        console.log(err);
      });
    this.navCtrl.setRoot(HomePage);
  }

  showToast(){
    let toast = this.toastCtrl
    .create({
      message: 'Your Water is on the way, Stay tunned !!!',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
   }
}
