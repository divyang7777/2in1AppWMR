import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const apiUrl: string = 'https://data.diatonic70.hasura-app.io/v1/query';

@Injectable()
export class DatabaseProvider {



  constructor(public http: HttpClient) {
    console.log('Hello there you are in database !');
  }

  
  addUser(data) {
    console.log(data);
    var body = {
      "type": "insert", 
      "args": {
        "table": "Home", 
        "objects": [{
          "username": data.name,
          "members": data.members,
          "summary": data.summary,
          "area": data.area
        }]
      }
    };

    console.log(JSON.stringify(body));
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl, JSON.stringify(body))
        .subscribe(res => {
          console.log(res);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  addIndustry(data) {
    console.log(data);
    var body = {
      "type": "insert", 
      "args": {
        "table": "Industry", 
        "objects": [{
          "i_name": data.name,
          "i_type": data.type,
          // "i_id": "123",
          "requirement": data.members,
          "summary": data.summary,
          "approval": "false"
          // "area": data.area
        }]
      }
    };

    console.log(JSON.stringify(body));
    return new Promise((resolve, reject) => {
      this.http.post(apiUrl, JSON.stringify(body))
        .subscribe(res => {
          console.log(res);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}